#include <stdio.h>

int main(void){
// Declarar variable cueNum.
	int cueNum;
// Preguntar a usuario número de cuenta.
	printf("%s", "Ingresa número de cuenta (-1 para salir): ");
// Obtener de usuario número de cuenta y asignar a variable cueNum.
	scanf("%d", &cueNum);

// Mientras cueNum sea diferente a -1.
	while (cueNum != -1){
// Declarar variable cueTip.
		char cueTip;
// Declarar variable preCan.
		unsigned int preCan;
// Declarar variable preCost.
		float preCost;
// Declarar variable total e inicializar (costo de factura) a 0.
		float total = 0.0;

// Preguntar a usuario tipo de cuenta.
		printf("%s", "Ingresa tipo de cuenta (r o n): ");
// Obtener de usuario tipo de cuenta y asignar a variable cueTip.
		scanf(" %c", &cueTip);


		switch (cueTip){
// Si cueTip es igual a 'r' o 'R' (tipo de cuenta residencial):
			case 'r':
			case 'R':
// Sumar 4.50 (valor de procesamiento a factura) a total.
				total += 4.5;
// Sumar 20.50 (valor de servicio básico a factura) a total.
				total += 20.5;
// Asignar 7.50 (valor unitario de canal premium) a preCost.
				preCost = 7.5;
				break;
// De lo contrario, si cueTip es igual a 'n' o 'N' (tipo de cuenta de
//      negocios):
			case 'n':
			case 'N':
// Sumar 15.00 (valor de procesamiento a factura) a total.
				total += 15;
// Sumar 75.00 (valor de servicio básico a factura) a total.
				total += 75.0;
// Declarar variable conCan.
				unsigned int conCan;
// Preguntar a usuario cantidad de conexiones.
				printf("%s", "Ingresa cantidad de conexiones: ");
// Obtener de usuario cantidad de conexiones y asignar a variable
//	 conCan.
				scanf("%u", &conCan);
// Si conCan es mayor a 10:
				if (conCan > 10){
// Sumo 5.00 (valor de conexiones adicionales) multiplicado por
//	    conCan - 10  a total.
					total += (conCan - 10) * 5.0;
				}
// Asignar 50.00 (valor unitario de canal premium) a preCost.
				preCost = 50.0;
				break;
// De lo contrario (tipo de cuenta erronea):
			default:
// Imprimir: error, tipo de cuenta erronea. Por favor intente nuevamente.
				printf("Error: %c no es un tipo de cuenta váli"\
						"do.\n", cueTip);
// Preguntar a usuario número de cuenta.
				printf("%s", "Ingresa número de cuenta (-1 par"\
					"a salir): ");
// Obtener de usuario número de cuenta y asignar a variable cueNum.
				scanf("%d", &cueNum);
// Finalizar iteración actual.
				continue;
		}

// Preguntar a usuario cantidad de canales premium.
		printf("%s", "Ingresa cantidad de canales premium: ");
// Obtener de usuario cantidad de canales premium y asignar a variable
//      preCan.
		scanf("%u", &preCan);

// Sumar preCan multiplicado por preCost a total.
		total += preCan * preCost;
// Imprimo cueNum y total.
		printf("\nNo. de Cuenta: %d\nCosto factura: $%.2f\n\n", cueNum, 
				total);
// Preguntar a usuario número de cuenta.
		printf("%s", "Ingresa número de cuenta (-1 para salir): ");
// Obtener de usuario número de cuenta y asignar a variable cueNum.
		scanf("%d", &cueNum);
	}
}
