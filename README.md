# Punto 1
Escriba un programa que permita calcular la factura de los clientes de la
compañía de cable local (De antemano, NO se sabe cuál es el número de
clientes). Hay dos tipos de clientes: residenciales y de negocios. Hay dos
tarifas para calcular una factura de cable: una para clientes residenciales y
la otra para clientes de negocios.

Para clientes residenciales se aplican las tarifas siguientes:
- Cargo por procesamiento de la factura: $4.50
- Cargo por servicio básico: $20.50
- Canales Premium: $7.50 por canal

Para clientes de negocios se aplican las tarifas siguientes:
- Cargo por procesamiento de la factura: $15.00
- Cargo por servicio básico: $75.00 por las primeras 10 conexiones; $5.00 por
  cada conexión adicional
- Canales Premium: $50.00 por canal para cualquier número de conexiones

El programa debe pedirle al usuario un número de cuenta (un entero) y un tipo
de cliente. Suponga que R o r denota cliente residencial y N o n denota cliente
de negocios.

Entrada: ingrese en el programa el número de cuenta del cliente, su tipo y el
número de canales Premium a los cuales está suscrito, y, en el caso de clientes
de negocios, el número de conexiones de servicio básico.

Salida: el número de cuenta del cliente y el importe de facturación.

Ejemplo de la ejecución del programa: 

```text
Este programa calcula una factura de cable.
Ingrese el número de cuenta: 12345
Ingrese el tipo de cliente: R o r (Residencial), N o n (Negocios): N
Ingrese el número de conexiones de servicio básico: 16
Ingrese el número de canales Premium: 8
Número de cuenta = 12345 Cantidad a pagar: $520.0
```

## Refinamiento 1

1. Obtener de usuario número de cuenta.
1. Mientras número de cuenta sea diferente a -1.
   1. Obtener de usuario tipo de cuenta (r, R, n o N).
   1. Si tipo de cuenta es residencial:
      1. Cargar valor de procesamiento a factura ($4.50).
      1. Cargar valor de servicio básico a factura ($20.50).
      1. Asignar valor unitario de canal premium ($7.50).
   1. De lo contrario, si tipo de cuenta es de negocios:
      1. Cargar valor de procesamiento a factura ($15.00).
      1. Cargar valor de servicio básico a factura ($75.00).
      1. Obtener de usuario cantidad de conexiones.
      1. Si cantidad de conexiones es mayor a 10:
         1. Cargo valor de conexiones adicionales (* $5.00). 
      1. Asignar valor unitario de canal premium($50.00).
   1. De lo contrario (tipo de cuenta erronea):
      1. Imprimir: error, tipo de cuenta erronea. Por favor intente nuevamente.
      1. Obtener de usuario número de cuenta.
      1. Finalizar iteración actual.
   1. Obtener de usuario cantidad de canales premium.
   1. Cargo valor de canales premium.
   1. Imprimo número de cuenta y total de factura.
   1. Obtener de usuario número de cuenta.

## Refinamimento 1 - Clase 20220301

1. Obtengo de usuario número de cuenta.
1. Mientras número de cuenta sea diferente a -1:
   1. Obtengo de usuario tipo de cuenta.
   1. Si tipo de cuenta es de negocios:
      1. Sumar costos fijos (procesamiento [$15.00] y servicio básico [$75.00])
	 de factura.
      1. Obtengo cantidad de conexiones.
      1. Si conexiones es mayor a 10:
	 1. `(conexiones - 10) * 5` sumo a total de factura.
      1. Asignar precio unitario de canal premium para negocios[$50.00].
   1. De lo contrario, si tipo de cuenta es residencial:
      1. Sumar costos fijos (procesamiento [$4.50] y servicio básico [$20.50])
	 de factura.
      1. Asignar precio unitario de canal premium para negocios [$7.50].
   1. De lo contrario:
      1. Imprimo: error, tipo de cuenta inválido.
   1. Obtener de usuario cantidad de canales premium.
   1. Calcular costo de canales premium sumar a factura.
   1. Imprimir número cuenta y valor de factura.
   1. Obtengo de usuario número de cuenta.

## Refinamiento 2

1. Preguntar a usuario número de cuenta.
1. Obtener de usuario número de cuenta y asignar a variable `cueNum`.
1. Mientras `cueNum` sea diferente a `-1`.
   1. Inicializar `total` (costo de factura) a `0`.
   1. Preguntar a usuario tipo de cuenta.
   1. Obtener de usuario tipo de cuenta y asignar a variable `cueTip`.
   1. Si `cueTip` es igual a `'r'` o `'R'` (tipo de cuenta residencial):
      1. Sumar `4.50` (valor de procesamiento a factura) a `total`.
      1. Sumar `20.50` (valor de servicio básico a factura) a `total`.
      1. Asignar `7.50` (valor unitario de canal premium) a `preCost`.
   1. De lo contrario, si `cueTip` es igual a `'n'` o `'N'` (tipo de cuenta de
      negocios):
      1. Sumar `15.00` (valor de procesamiento a factura) a `total`.
      1. Sumar `75.00` (valor de servicio básico a factura) a `total`.
      1. Preguntar a usuario cantidad de conexiones.
      1. Obtener de usuario cantidad de conexiones y asignar a variable
	 `conCan`.
      1. Si `conCan` es mayor a 10:
	 1. Sumo `5.00` (valor de conexiones adicionales) multiplicado por
	    `conCan` a `total`.
      1. Asignar `50.00` (valor unitario de canal premium) a `preCost`.
   1. De lo contrario (tipo de cuenta erronea):
      1. Imprimir: error, tipo de cuenta erronea. Por favor intente nuevamente.
      1. Preguntar a usuario número de cuenta.
      1. Obtener de usuario número de cuenta y asignar a variable `cueNum`.
      1. Finalizar iteración actual.
   1. Preguntar a usuario cantidad de canales premium.
   1. Obtener de usuario cantidad de canales premium y asignar a variable
      `preCan`.
   1. Sumar `preCan` multiplicado por `preCost` a `total`.
   1. Imprimo `cueNum` y `total`.
   1. Preguntar a usuario número de cuenta.
   1. Obtener de usuario número de cuenta y asignar a variable `cueNum`.

## Refinamiento 3

1. Declarar variable `cueNum`.
1. Preguntar a usuario número de cuenta.
1. Obtener de usuario número de cuenta y asignar a variable `cueNum`.
1. Mientras `cueNum` sea diferente a `-1`.
   1. Declarar variable `cueTip`.
   1. Declarar variable `preCan`.
   1. Declarar variable `preCost`.
   1. Declarar variable `total` e inicializar (costo de factura) a `0`.
   1. Preguntar a usuario tipo de cuenta.
   1. Obtener de usuario tipo de cuenta y asignar a variable `cueTip`.
   1. Si `cueTip` es igual a `'r'` o `'R'` (tipo de cuenta residencial):
      1. Sumar `4.50` (valor de procesamiento a factura) a `total`.
      1. Sumar `20.50` (valor de servicio básico a factura) a `total`.
      1. Asignar `7.50` (valor unitario de canal premium) a `preCost`.
   1. De lo contrario, si `cueTip` es igual a `'n'` o `'N'` (tipo de cuenta de
      negocios):
      1. Sumar `15.00` (valor de procesamiento a factura) a `total`.
      1. Sumar `75.00` (valor de servicio básico a factura) a `total`.
      1. Declarar variable `conCan`.
      1. Preguntar a usuario cantidad de conexiones.
      1. Obtener de usuario cantidad de conexiones y asignar a variable
	 `conCan`.
      1. Si `conCan` es mayor a 10:
	 1. Sumo `5.00` (valor de conexiones adicionales) multiplicado por
	    `conCan` a `total`.
      1. Asignar `50.00` (valor unitario de canal premium) a `preCost`.
   1. De lo contrario (tipo de cuenta erronea):
      1. Imprimir: error, tipo de cuenta erronea. Por favor intente nuevamente.
      1. Preguntar a usuario número de cuenta.
      1. Obtener de usuario número de cuenta y asignar a variable `cueNum`.
      1. Finalizar iteración actual.
   1. Preguntar a usuario cantidad de canales premium.
   1. Obtener de usuario cantidad de canales premium y asignar a variable
      `preCan`.
   1. Sumar `preCan` multiplicado por `preCost` a `total`.
   1. Imprimo `cueNum` y `total`.
   1. Preguntar a usuario número de cuenta.
   1. Obtener de usuario número de cuenta y asignar a variable `cueNum`.

# Punto 2

Una institución universitaria tiene 1000 estudiantes y 1000 casilleros, un
casillero para cada estudiante. En el primer día de clases el rector propone el
siguiente juego:

a. Pide a un primer estudiante que abra todos los casilleros.
a. Luego le pide a un segundo estudiante que cierre todos los casilleros con
   número par.
a. Al tercer estudiante le pide que verifique cada tercer casillero. Si está
   abierto, el estudiante lo cierra; si está cerrado, el estudiante lo abre.
a. A un cuarto estudiante se le pide que verifique cada cuarto casillero. Si
   está abierto, el estudiante lo cierra; si está cerrado, el estudiante lo
   abre.
a. Los estudiantes restantes continúan este juego. En general, el enésimo
   estudiante verifica cada enésimo casillero. Si el casillero está abierto, el
   estudiante lo cierra; si está cerrado, el estudiante lo abre.

Después de que todos los estudiantes han tomado sus turnos, algunos de los
casilleros están abiertos y algunos están cerrados.

Escriba un programa que le solicite al usuario ingresar el número de casilleros
en una institución. El programa indica como salida los números de los
casilleros que quedan abiertos y la cantidad de casilleros abiertos.

Para resolver el problema, un estudiante muy juicioso observó que, si el número
de divisores positivos de un número de casillero es impar, entonces al final
del juego el casillero está abierto. Si el número de divisores positivos de un
número de casillero es par, entonces al final del juego el casillero está
cerrado.

Por ejemplo, si se considera el casillero número 100. Este lo visitan los
estudiantes número 1, 2, 4, 5, 10, 20, 25, 50 y 100. Estos son divisores
positivos de 100 y como son nueve divisores positivos, entonces, el casillero
queda abierto. De manera similar, el casillero número 30 lo visitan los
estudiantes número 1, 2, 3, 5, 6, 10, 15 y 30. Por lo cual, el casillero queda
cerrado.

## Refinamiento 1

1. Obtener de usuario cantidad de casilleros.
1. Para número de casillero desde `1`; hasta cantidad de casilleros; incrementando `1`:
   1. Para divisor desde `1`; hasta número de casillero; incrementando `1`:
      1. Si división entera entre número de casillero y divisor es `0`:
         1. Incremento cantidad de divisores en `1`. 
   1. Si cantidad de divisores es impar:
      1. Imprimir número de casillero abierto.
      1. Incrementar cantidad de casilleros abiertos en `1`.
1. Imprimir cantidad de casilleros abiertos.

## Refinamiento 2

1. Preguntar a usuario cantidad de casilleros.
1. Obtener de usuario cantidad de casilleros y asignar a variable `casCan`.
1. Para `casNum` desde `1`; hasta `casNum` menor o igual a `casCan`; `casNum`
   incremento `1`:
   1. Inicializo `canDiv` (cantidad divisores positivos) a 0.
   1. Para `div` desde `1`; hasta `div` menor o igual a `casNum`; `div`
      incremento `1`:
      1. Si `casNum / div == 0`:
	 1. Incremento `canDiv` en `1`. 
   1. Si `canDiv` es impar:
      1. Imprimir `casNum`.
      1. Incrementar `casAbi` en `1`.
1. Imprimir `casAbi`.

## Refinamiento 3


1. Declaro variable `casCan` (cantidad de casilleros que tiene la institución).
1. Declaro variable `casAbi` (cantidad de casilleros que quedan abiertos).
1. Preguntar a usuario cantidad de casilleros.
1. Obtener de usuario cantidad de casilleros y asignar a variable `casCan`.
1. Para `casNum` desde `1`; hasta `casNum` menor o igual a `casCan`; `casNum`
   incremento `1`:
   1. Declaro e inicializo `canDiv` (cantidad divisores positivos) a 0.
   1. Para `div` desde `1`; hasta `div` menor o igual a `casNum`; `div`
      incremento `1`:
      1. Si `casNum / div == 0`:
	 1. Incremento `canDiv` en `1`. 
   1. Si `canDiv` es impar:
      1. Imprimir `casNum`.
      1. Incrementar `casAbi` en `1`.
1. Imprimir `casAbi`.

