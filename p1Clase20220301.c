#include <stdio.h>

int main(void){
// Declarar variable cueNum.
	int cueNum;
// Preguntar a usuario número de cuenta.
	printf("Ingrese número de cuenta: ");
// Obtener de usuario número de cuenta y asignar a variable cueNum.
	scanf("%d", &cueNum);

// Mientras cueNum sea diferente a -1.
	while(cueNum != -1){
// Declarar variable cueTip.
		char cueTip;
// Declarar variable preCan.
		int preCan;
// Declarar variable preCost.
		float preCost;
// Declarar variable total e inicializar (costo de factura) a 0.
		float total = 0;
// Preguntar a usuario tipo de cuenta.
		printf("Ingresa tipo de cuenta: ");
// Obtener de usuario tipo de cuenta y asignar a variable cueTip.
		scanf(" %c", &cueTip);
// Si cueTip es igual a 'r' o 'R' (tipo de cuenta residencial):
		if (cueTip == 'r' || cueTip == 'R'){
// Sumar 4.50 (valor de procesamiento a factura) a total.
			total += 4.5;
// Sumar 20.50 (valor de servicio básico a factura) a total.
			total += 20.5;
// Asignar 7.50 (valor unitario de canal premium) a preCost.
			preCost = 7.50;
		}
// De lo contrario, si cueTip es igual a 'n' o 'N' (tipo de cuenta de
//      negocios):
		else if (cueTip == 'n' || cueTip == 'N'){
// Sumar 15.00 (valor de procesamiento a factura) a total.
			total += 15;
// Sumar 75.00 (valor de servicio básico a factura) a total.
			total += 75;
// Declarar variable conCan.
			int conCan;
// Preguntar a usuario cantidad de conexiones.
			printf("Ingrese la cantidad de conexiones: ");
// Obtener de usuario cantidad de conexiones y asignar a variable
//	 conCan.
			scanf("%d", &conCan);
// Si conCan es mayor a 10:
			if (conCan > 10){
// Sumo 5.00 (valor de conexiones adicionales) multiplicado por
//	    conCan a total.
				total += 5 * (conCan - 10);
			}
// Asignar 50.00 (valor unitario de canal premium) a preCost.
			preCost = 50;
		}
// De lo contrario (tipo de cuenta erronea):
		else{
// Imprimir: error, tipo de cuenta erronea. Por favor intente nuevamente.
			printf("Tipo de cuenta erronea\n");
// Preguntar a usuario número de cuenta.
			printf("Ingrese número de cuenta: ");
// Obtener de usuario número de cuenta y asignar a variable cueNum.
			scanf("%d", &cueNum);
// Finalizar iteración actual.
			continue;
		}
// Preguntar a usuario cantidad de canales premium.
		printf("Ingrese la cantidad de canales premium: ");
// Obtener de usuario cantidad de canales premium y asignar a variable
//      preCan.
		scanf("%d", &preCan);
// Sumar preCan multiplicado por preCost a total.
		total += preCan * preCost;
// Imprimo cueNum y total.
		printf("Número de cuenta: %d\nTotal factura: %.2f\n", cueNum,
				total);
// Preguntar a usuario número de cuenta.
		printf("Ingrese número de cuenta: ");
// Obtener de usuario número de cuenta y asignar a variable cueNum.
		scanf("%d", &cueNum);
	}
}
