#include <stdio.h>

int main(void){
// Declaro variable casCan (cantidad de casilleros que tiene la institución).
	unsigned int casCan;
// Declaro variable casAbi (cantidad de casilleros que quedan abiertos).
	unsigned int casAbi = 0;
// Preguntar a usuario cantidad de casilleros.
	printf("Ingresa cantidad de casilleros (mayor a 0): ");
// Obtener de usuario cantidad de casilleros y asignar a variable casCan.
	scanf("%u", &casCan);
// Para casNum desde 1; hasta casNum menor o igual a casCan; casNum
//   incremento 1:
	for (unsigned int casNum = 1; casNum <= casCan; casNum++){

// Declaro e inicializo canDiv (cantidad divisores positivos) a 0.
		unsigned int canDiv = 0;
// Para div desde 1; hasta div menor o igual a casNum; div
//      incremento 1:
		for (unsigned int div = 1; div <= casNum; div++){
// Si casNum / div == 0:
			if(casNum % div == 0){
// Incremento canDiv en 1. 
				canDiv++;
			}
		}

// Si canDiv es impar:
		if (canDiv % 2 != 0){
// Imprimir casNum.
			printf("%u ", casNum);
// Incrementar casAbi en 1.
			casAbi++;
		}
	}

	// Imprimo cambio de línea
	puts("");
// Imprimir casAbi.
	printf("Casilleros abiertos: %u\n", casAbi);
}
